package Servidor;


import java.net.Socket;



public class ORBThreads implements Runnable {
	
	private Socket sc;
	private ISkeleton sk;
	public ORBThreads(Socket sc, ISkeleton sk) {
	
		this.sc = sc;
		this.sk = sk;
	}
	public void run() {
	
		sk.process(sc);
		
	}
	

}
