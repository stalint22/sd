package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Hashtable;

public class SkeletonAgenda implements ISkeleton {

	
		static final int iid = 1;
		static Hashtable<Integer, Agenda> objTable = new Hashtable<Integer, Agenda>(); 
		int objsAgenda = -1; //contador de objetos creados inicialmente 0
		int codOp = -1;
		DataInputStream canalEntrada;
		DataOutputStream canalSalida;
		
		public int getIid(){
			return iid;
		}

		public void process(Socket sc) {
			int idObjeto = -1;
			Agenda agendaActual = null;
			try {
				//Referencia a los canales de E/S del socket
				canalEntrada = new DataInputStream(sc.getInputStream());
				canalSalida = new DataOutputStream(sc.getOutputStream());
				//codigo de operaci�n
				codOp = canalEntrada.readInt();
				
				switch (codOp) {
				case 0:
					//crear obj Agenda - Devolver objId
					objsAgenda++;
					//Intersecci�n del nuevo objeto agenda en la tabla de agendas
					objTable.put(new Integer(objsAgenda), new Agenda());
					canalSalida.writeBoolean(true);
					canalSalida.writeInt(objsAgenda);
					break;
				case 1:
					//Operaci�n asociar
					//1� Obtener la agenda asociada al identificador de objeto
					//2�Llamar al m�todo asociar por medio de su agenda
					
					idObjeto = canalEntrada.readInt();
					agendaActual.asociar(canalEntrada.readUTF(),new Integer(canalEntrada.readInt()));
					canalSalida.writeBoolean(true);//asociaci�n correcta
					break;
					
				case 2://Obtener. Si la clave existe devolvemos el valor.
						//Si la calve no existe, devolvemos una ecepci�n
					
					//Obtener de la tabla de Agendas la Agenda
					
					idObjeto = canalEntrada.readInt();
					agendaActual = (Agenda) objTable.get(idObjeto);
					try {
					
						int valor = agendaActual.obtener(canalEntrada.readUTF());
						canalSalida.writeBoolean(true); //okey existe la clave
						canalSalida.writeInt(valor);
					} catch (Exception e) {
						// TODO: handle exception
						canalSalida.writeBoolean(false);;//error no existe la clave buscada
						
						//Se captura la excepci�n y se envia serializada
						
						ObjectOutputStream oos = new ObjectOutputStream(sc.getOutputStream());
						oos.writeObject(e);
					}
									
					break;
					
				}
				sc.close();
			} catch (IOException e) {
				// TODO: handle exception
				System.out.println(e.toString());
			}
		}
	
	
		
	
}
