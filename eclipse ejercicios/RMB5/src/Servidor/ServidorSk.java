package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;

public class ServidorSk {
	//variables
	ServerSocket ss;
	Socket sc;
	DataInputStream canalentrada;
	DataOutputStream canalsalida;
	Hashtable<Integer, ISkeleton> skeletonHash = new Hashtable<Integer, ISkeleton>();
	int iid=0;//identificador interfaz
	static int serverport = 9999;
	
//constructor
	public ServidorSk(int serverport) {
	this.serverport = serverport;
	}
	
	public void addSkeleton(ISkeleton sk){
		skeletonHash.put(iid, sk);
		iid++;
		
		//skeletonHash.put(sk.getClass(iid), sk);
	}
	
	public ISkeleton getSkeleton(int iid) {
		return skeletonHash.get(iid);
	}
	
	public void arrancarServidor(){
		//ISkeleton skeleton;
		
		try {
			ss = new ServerSocket(serverport);
			System.out.println("Creado server del socket");
			while(true){
				sc = ss.accept();
				canalentrada = new DataInputStream(sc.getInputStream());
				canalsalida = new DataOutputStream(sc.getOutputStream());
				
				//indentificador de interface
				iid = canalentrada.readInt();
				ISkeleton sk = (ISkeleton) (skeletonHash.get(iid));
				ORBThreads t = new ORBThreads(sc,sk);
				Thread th = new Thread(t);
				th.start();
				//skeleton = getSkeleton(iid);
				sk = getSkeleton(iid);
				sk.process(sc);
				
				sc.close();
			}
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println(e.toString());
		}
	}
	
	public static void main(String[] args){
		ServidorSk servRep = new ServidorSk(serverport);
		SkeletonAgenda skAgenda = new SkeletonAgenda();
		servRep.addSkeleton(skAgenda);
		
		servRep.arrancarServidor();
		
		
		
	}
}
