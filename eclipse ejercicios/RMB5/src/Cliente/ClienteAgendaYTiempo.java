package Cliente;

public class ClienteAgendaYTiempo {
	public static void main(String[] args) {
		Time horaActual = new Time();
		System.out.println("Hora Actual Servidor " + horaActual.getHour() + ":"+
				horaActual.getMinute()+":"+ horaActual.getSeconds());
		Agenda agendaTelefonica = new Agenda();
		agendaTelefonica.asociar("Juan", 66756677);
		System.out.println("Telefono Juan = " + agendaTelefonica.obtener("Juan"));
	}
}
