package Cliente;

interface IRepositorio {
	// Se asocia un valor entero a una cadena
	public void asociar(String key, int d);
	// Se obtiene el valor entero asociado previamente a la
	// cadena especificada. Eleva una excepci�n si la calve no existe
	public int obtener(String key) throws Exception;
}

