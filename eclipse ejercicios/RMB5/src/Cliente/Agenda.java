package Cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Agenda implements IRepositorio{
	static String host = "localhost";
	static int port = 9999;
	static int iid =0;//identificador interfaz
	int oid = 0;//idebtificador objeto
	
	public Agenda(){
		//creamos el socket
		try {
			Socket s = new Socket(host,port);
			//canals de entrada y salida
			DataInputStream canalentrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalsalida = new DataOutputStream(s.getOutputStream());
			
			canalsalida.writeInt(iid);
			canalsalida.writeInt(0);//c�digo de operaci�n del objeto
			
			if(canalentrada.readBoolean()){
				oid = canalentrada.readInt();
				System.out.println("Cliente, se ha creado el objeto agenda");
			}
			else	System.out.println("No se ha creado el objeto");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	public void asociar(String key, int v){
		try {
			//creamos conexion al socket
			Socket s = new Socket(host,port);
			
			//canales de entrada y salida
			DataInputStream canalentrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalsalida = new DataOutputStream(s.getOutputStream());
			
			canalsalida.writeInt(iid);
			canalsalida.writeInt(1);
			canalsalida.writeInt(oid);
			canalsalida.writeUTF(key);
			canalsalida.writeInt(v);
			System.out.println("Cliente, se ha creado el metodo asiociar");
			
			//comprobacion de error
			
			if(!canalentrada.readBoolean())
				System.out.println("Error");
			canalentrada.close();
			canalsalida.close();
			s.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int obtener (String key){
		int v;
		
		try {
			//conexion socket server
			Socket s = new Socket(host,port);
			DataInputStream canalentrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalsalida = new DataOutputStream(s.getOutputStream());
			
			canalsalida.writeInt(iid);
			canalsalida.writeInt(2);//metodo asociado a c�digo de operaci�n
			canalsalida.writeInt(oid);//objeto de la agenda
			canalsalida.writeUTF(key);
			
			System.out.println("Cliente, se ha creado obtener");
			
			if(!canalentrada.readBoolean())
				System.out.println("Error");
			v= canalentrada.readInt(); //valor devuelto
			
			canalentrada.close();
			canalsalida.close();
			s.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			v = -1;
		}
		return v;
		
	}
	
	public void tiempo(int getHour, int getMinute, int getSecond){
		try {
			//conexion socket
			Socket s =new Socket();
			DataInputStream canalentrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalsalida = new DataOutputStream(s.getOutputStream());
			
			canalsalida.writeInt(iid);
			canalsalida.writeInt(oid);
			canalsalida.writeInt(getHour);
			canalsalida.writeInt(getMinute);
			canalsalida.writeInt(getSecond);
			
			System.out.println("Se ha enviado el tiempo ");
			
			//cerramos canales y socet de conexion
			
			canalentrada.close();
			canalsalida.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
