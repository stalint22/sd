package Server;
/*
 * Interfaz de esqueletos
 */

import java.net.Socket;

public interface ISkeleton {
	public int getIid();
	
	public void process(Socket sc);

}
