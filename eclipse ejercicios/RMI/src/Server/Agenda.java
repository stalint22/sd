package Server;
/*
 * Implementa el servicio agenda
 */

import java.util.Hashtable;

public class Agenda implements IAgenda {
	private Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
	
	public void asociar(String s, int v){
		ht.put(s, new Integer(v));
	}

	
	public int obtener(String s) throws Exception {
		Integer value = ((Integer) ht.get(s));
		
		if (value == null) throw new Exception();
		else 
			return value.intValue();
		
	}
	
}
