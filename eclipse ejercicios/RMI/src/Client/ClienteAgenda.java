package Client;

public class ClienteAgenda {

	public static void main(String[] args){
		Agenda agendaTelefonica = new Agenda();
		Agenda agendaClaves = new Agenda();
		agendaTelefonica.asociar("Simon", 65484578);
		agendaClaves.asociar("Simon",123);
		try{
		System.out.println("Telefono Simon = " + agendaTelefonica.obtener("Simon"));
		System.out.println("Clave maquina C3po = "+ agendaClaves.obtener("C3po"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
