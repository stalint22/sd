package Cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Agenda implements IRepositorio {
	static String host = "localhost";
	static int port = 9999;
	static int iid = 0; //Identificador de la interfaz. Interfaz Agenda = 0
	int oid = 0; //identificador de objeto dentro de la clase Agenda
	//public Agenda(int iid, int oid)
	public Agenda() { // Constructor
		// A rellenar por el alumno
		// crea socket de cliente

		try {
			//Creamos una conexion socket servidor
			Socket s = new Socket(host, port);
			//Creamos los canales de Entrada y Salida
			DataInputStream canalEntrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalSalida = new DataOutputStream(s.getOutputStream());

			canalSalida.writeInt(iid);
			canalSalida.writeInt(1); //Cod Op crear agenda = 0
			if (canalEntrada.readBoolean()){
				oid = canalEntrada.readInt(); //se lee codigo del objeto
				System.out.println(" Cliente, Se ha creado el objeto Agenda");
			}

			else
				System.out.println("No se ha creado el objeto");

			// cierra el socket
			s.close();

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}


	public void asociar(String key, int v) {
		//A rellenar por el alumno
		try {
			//Creamos una conexion socket servidor
			Socket s = new Socket(host, port);
			//Creamos los canales de Entrada y Salida
			DataInputStream canalEntrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalSalida = new DataOutputStream(s.getOutputStream());


			canalSalida.writeInt(iid);
			canalSalida.writeInt(2);
			canalSalida.writeInt(oid);
			canalSalida.writeUTF(key);
			canalSalida.writeInt(v);
			System.out.println(" Cliente, Se ha creado asociar");

			if(!canalEntrada.readBoolean()) System.out.println("Error");

			canalEntrada.close();
			canalSalida.close();
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public int obtener(String key) {
		//A rellenar por el alumno
		int v;
		try {
			//Creamos una conexion socket servidor
			Socket s = new Socket(host, port);
			//Creamos los canales de Entrada y Salida
			DataInputStream canalEntrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalSalida = new DataOutputStream(s.getOutputStream());


			canalSalida.writeInt(iid);
			canalSalida.writeInt(3); //metodo a invocar
			canalSalida.writeInt(oid); //objeto de agenda

			canalSalida.writeUTF(key);


			System.out.println(" Cliente, Se ha creado obtener");

			if(!canalEntrada.readBoolean()) System.out.println("Error");
			v=canalEntrada.readInt(); //valor devuelto

			canalEntrada.close();
			canalSalida.close();
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
			v=-1;
		}
		return v;
	}
	public void tiempo (int getHour, int getMinute, int getSenconds) {

		try {
			//Creamos una conexion socket servidor
			Socket s = new Socket(host, port);
			//Creamos los canales de Entrada y Salida
			DataInputStream canalEntrada = new DataInputStream(s.getInputStream());
			DataOutputStream canalSalida = new DataOutputStream(s.getOutputStream());

			canalSalida.writeInt(iid);

			canalSalida.writeInt(oid); //objeto de agenda
			canalSalida.writeInt(getHour);
			canalSalida.writeInt(getMinute);
			canalSalida.writeInt(getSenconds);

			System.out.println(" Cliente, Se ha enviado el tiempo");

			canalEntrada.close();
			canalSalida.close();
			s.close();

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}	
}