package Servidor;

import java.io.*;
import java.net.Socket;
import java.util.Hashtable;

public class SkeletonAgenda implements ISkeleton {
	//tabla de objetos de tipo Agenda
	static final int iid = 1;
	int objId = -1; //lleva la cuenta de objetos de Agenda creados.
	
	Hashtable<Integer, Agenda> objectHash = new Hashtable<Integer, Agenda>();

	Agenda objetoRep  = null;
	DataInputStream canalEntrada;
	DataOutputStream canalSalida;

	public void addObject(Object obj) {
		objId++;
		objectHash.put(objId, (Agenda) obj);
		
	}

	public Agenda  getObject(int objid) {
		return objectHash.get(objid);
	}
	
	public int getIid() {
		return iid;
	}

	public void process(Socket sc) {
		//peticion iid, codOp, objId, par1, ... parn
		//respuesta boolean, if true resultado. 
		int id = -1;
		try {
			// leer el num. m�todo a invocar
			int numMethod = canalEntrada.readInt();

			switch (numMethod) {

			case 1: //crear objeto de agenda
			{
				System.out.println("Crear agenda");
				addObject(new Agenda());
				//devolver al cliente el objid
				canalSalida.writeBoolean(true);
				canalSalida.writeInt(objId);

				break;
			}

			case 2: 
			{ 
				System.out.println("Asociar" );	
				//leer el objId
				id = canalEntrada.readInt();
				//obtenerlo de la tabla
				Agenda objetoRep = objectHash.get(id);
				String clave = canalEntrada.readUTF();
				int valor = canalEntrada.readInt();				
				objetoRep.asociar(clave, valor);
				
				canalSalida.writeBoolean(true);


				break;
			}


			case 3: 
			{
				System.out.println("Obtener");
				//leer el objId
				id = canalEntrada.readInt();
				//otenerlo de la tabla
				Agenda objetoRep = objectHash.get(id);
				String clave = canalEntrada.readUTF();
				int valor=objetoRep.obtener(clave);

				canalSalida.writeBoolean(true);
				canalSalida.writeInt(valor);


				break;
			}
			default:
				System.out.println("Invocacion erronea");
				break;
			}
			canalEntrada.close();
			canalSalida.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}