package Servidor;

import java.io.*;
import java.net.Socket;
public interface ISkeleton {
	public int getIid();
	public void process(Socket sc);
}