package Servidor;

import java.util.Hashtable;

public class Agenda implements IRepositorio {
	// Se implementa la interfaz IRepositorio con una tabla hash
	private Hashtable<String, Integer> ht = new Hashtable<String, Integer>();

	public void asociar(String s, int v) {
		ht.put(s, new Integer(v));
	}

	public int obtener(String s) {
		return ((Integer) ht.get(s)).intValue();
	}
}
