package Server;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;

public class Servidor {

	public static void main(String[] args) {
		ServerSocket ss;
		Socket sc;
		int iid = -1;
		
		Hashtable<Integer, ISkeleton> skTable = new Hashtable<Integer,ISkeleton>();
		DataInputStream canalEntrada = null;
		try {
			//se da de alta al servicio Agenda.
			ss= new ServerSocket(1237);
									
			//se crea el esqueleto.
			
			SkeletonAgenda skAgenda = new SkeletonAgenda();
			
			//se da de alta ne la tabla de servicios
			skTable.put(skAgenda.getIid(), skAgenda);
			
			while (true) {
				sc= ss.accept();
				System.out.println("Realizando conexi�n ...");
				canalEntrada = new DataInputStream(sc.getInputStream());
				
				//se lee el identificador de servicio
				iid = canalEntrada.readInt();
				(skTable.get(iid)).process(sc);
				
				
				sc.close();
				
			}
			
		} catch (Exception e) {
		
			e.printStackTrace();
		}

	}

}
