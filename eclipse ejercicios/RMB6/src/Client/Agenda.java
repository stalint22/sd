package Client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Agenda implements IAgenda{
	
	String host = "localhost";
	int port = 1237;
	int objId = -1;
	final int iid = -1;
	
	public Agenda(){
		Socket sc;
		DataInputStream canalEntrada;
		DataOutputStream canalSalida;
		boolean ok = false;
		
		
		try {
			sc = new Socket(host, port);
			
			canalSalida = new DataOutputStream(sc.getOutputStream());
			canalEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(iid);
			canalSalida.writeInt(1);//CODIFO DE OPERACIÓN CREAR OBJETO AGENDA
			
			ok = canalEntrada.readBoolean();
			objId = canalEntrada.readInt();
			
			sc.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void asociar(String s, int v) {
		Socket sc;
		DataInputStream canalEntrada;
		DataOutputStream canalSalida;
		String host = "localhost";
		boolean ok = false;
		try {
			sc = new Socket(host, port);
			canalSalida = new DataOutputStream(sc.getOutputStream());
			canalEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(iid);
			canalSalida.writeInt(2);
			canalSalida.writeInt(objId);//SOBRE LA AGENDA OBJiD
			canalSalida.writeUTF(s);
			canalSalida.writeInt(v);//valor o clave
			
			ok=canalEntrada.readBoolean();
			sc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int obtener(String s) throws Exception {
		
		Socket sc;
		DataInputStream canalEntrada;
		DataOutputStream canalSalida;
		int valor = -1;
		boolean ok = false;		
		try {
			sc = new Socket(host, port);
			
			canalSalida = new DataOutputStream(sc.getOutputStream());
			canalEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(iid);
			canalSalida.writeInt(3);
			canalSalida.writeInt(objId);
			canalSalida.writeUTF(s);
			ok = canalEntrada.readBoolean();
			if(ok){
				valor = canalEntrada.readInt();
			}
			else{
				ObjectInputStream ois = new ObjectInputStream(sc.getInputStream());
				Exception e = (Exception) ois.readObject();
				throw e;
			}
		sc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valor;
	}
	
}
