package Agenda3.cliente;
//import Agenda3.servidor.Agenda3;
public class ClienteAgenda3 {

	public static void main(String[] args) {
		Agenda3 agendaTelefonica = new Agenda3();
		Agenda3 guiaClaves = new Agenda3();
		agendaTelefonica.asociar("Juan", 292929292);
		guiaClaves.asociar("Moodle",23323);
		guiaClaves.asociar("Juan",3333);
		System.out.println("Telefono Juan = " + agendaTelefonica.obtener("Juan"));
		System.out.println("Clave Moodle = "+ guiaClaves.obtener("Moodle"));
		System.out.println("Clave Juan = "+ guiaClaves.obtener("Juan"));
	}

}
