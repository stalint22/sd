package Agenda3.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;

public interface ISkeleton {
	//Esqueleto de servicio --c�digo  que identifica  e invoca los m�todos
	//y par�metros  de una interfaz determinada.
	public void process(DataInputStream canalEntrada,DataOutputStream canalSalida);

}
