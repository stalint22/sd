package Agenda3.servidor;

import java.io.*;
import java.util.Hashtable;

public class SkeletonAgenda implements ISkeleton {
	//tabla de objetos de tipo Agenda
	int objId = -1; //lleva la cuenta de objetos de Agenda creados.
	Hashtable<Integer, Agenda3> objectHash = new Hashtable<Integer, Agenda3>();

	Agenda3 objetoRep  = null;

	public void addObject(Object obj) {
		objId++;
		objectHash.put(objId, (Agenda3) obj);
		
	}

	public Agenda3  getObject(int objid) {
		return objectHash.get(objid);
	}

	public void process(DataInputStream canalEntrada,DataOutputStream canalSalida) {
		//peticion iid, codOp, objId, par1, ... parn
		//respuesta boolean, if true resultado. 
		try {
			// leer el num m�tofdo a invocar
			int numMethod = canalEntrada.readInt();

			switch (numMethod) {

			case 0: //crear objeto de agenda
			{
				System.out.println("Crear agenda");
				addObject(new Agenda3());
				//devolver al cliente el objid
				canalSalida.writeBoolean(true);
				canalSalida.writeInt(objId);

				break;
			}

			case 1: 
			{ 
				System.out.println("Asociar" );	
				//leer el objId
				int id = canalEntrada.readInt();
				//otenerlo de la tabla
				Agenda3 objetoRep = getObject(id);
				String clave = canalEntrada.readUTF();
				int valor = canalEntrada.readInt();
				
				objetoRep.asociar(clave, valor);
				canalSalida.writeBoolean(true);


				break;
			}


			case 2: 
			{
				System.out.println("Obtener");
				//leer el objId
				int id = canalEntrada.readInt();
				//otenerlo de la tabla
				Agenda3 objetoRep = objectHash.get(id);
				String clave = canalEntrada.readUTF();
				int valor=objetoRep.obtener(clave);

				canalSalida.writeBoolean(true);
				canalSalida.writeInt(valor);


				break;
			}
			default:
				System.out.println("Invocacion erronea");
				break;
			}
			canalEntrada.close();
			canalSalida.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}