package Agenda3.servidor;

interface IRepositorio3 {
	// Se asocia un valor entero a una cadena
	public void asociar(String v, int d);

	// Se obtiene el valor entero asociado previamente a la
	// cadena especificada.
	public int obtener(String key);
}

