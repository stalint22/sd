package Agenda3.servidor;

import java.io.*;
import java.net.*;
import java.util.*;

public class ServidorSk {
	ServerSocket ss;
	Socket sc;
	DataInputStream canalEntrada;
	DataOutputStream canalSalida;
	Hashtable<Integer, ISkeleton> skeletonHash = new Hashtable<Integer, ISkeleton>();
	int iid = 0;
	int serverPort;

	ServidorSk(int serverPort) {
		this.serverPort = serverPort;
	}

	public void addSkeleton(ISkeleton sk) {
		skeletonHash.put(iid, sk);
		iid++;
	}

	public ISkeleton getSkeleton(int iid) {
		return skeletonHash.get(iid);
	}

	public void arrancaServidor() {
		// A rellenar por el alumno
		ISkeleton skeleton;

		try {
			ss = new ServerSocket(serverPort);
			System.out.println("Creado servidor del Socket");
			while (true) {
				sc = ss.accept();
				DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
				DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());

				iid = canalEntrada.readInt();

				skeleton = getSkeleton(iid);

				skeleton.process(canalEntrada, canalSalida);

				sc.close();
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	public static void main(String[] args) {
		ServidorSk servRep = new ServidorSk(9999);
		SkeletonAgenda skAgenda = new SkeletonAgenda();
		servRep.addSkeleton(skAgenda);
		servRep.arrancaServidor();
	}
}