package examen;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class clienteMonohilo {
		String fileServiceHost = null;
		int fileServicePort = -1;
		Socket socket;
		String leido;

		public  clienteMonohilo(String fileServiceHost, int fileServicePort) {
		this.fileServiceHost = fileServiceHost;
		this.fileServicePort = fileServicePort;
		}
		public void remoteCopy(String localFileName, String remoteFileName)
		throws IOException {
		// 1.pendiente de implementar
		//Abre el fichero local a transmitir (localFileName)
			
			//Como se har� con Filerreader y bufferreader para enviarlo al outputstream?
			//FileReader origen = new FileReader(localFileName);
			//BufferedReader brorigen = new BufferedReader(origen);

		//Establece conexion con el servidor, escribe en el socket el nombre que tendr� este
		//fichero en el nodo remoto (remoteFileName)
			socket = new Socket (fileServiceHost,fileServicePort);
			
	
			BufferedReader in = new BufferedReader(new FileReader(localFileName));
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			while ((leido = in.readLine()) != null) {
				out.write(leido);
			}
			socket.close();
			in.close();
			out.close();
		//Lee el contenido del fichero local y lo escribe en el socket, hasta alcanzar el eof.
		//cierra recursos
			
			//origen.close();
		}
		public static void main(String[] args) throws IOException {
			clienteMonohilo myFileService = new clienteMonohilo("LocalHost",1234);
		try {
		//por ejemplo
		myFileService.remoteCopy("./src/io/PractEcho.java", "./src/io/PractEcho.java");
		} catch (FileNotFoundException e) {
		System.out.println("Client: File Not Found" + e.toString());
		} catch (UnknownHostException unknownHostEx) {
		System.out.println("Client:unknownHost:"+ unknownHostEx.getMessage());
		}
		}
		}

