package examen;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class FileService implements Runnable{
	
	//Rellene dicha clase con las variables y par�metros y m�todos que considere necesarios
	BufferedReader CanalEntrada;
	Socket usrConnection = null;
	int backupAppPort=1234;


	
	public FileService(Socket usrConnection) {
		this.usrConnection = usrConnection;
	}

	public void remoteCopy(Socket usrConnection) throws IOException {
	//Se supone el mismo contenido que en la versi�n monohilo.
		//Lee de la conexi�n de usuario, el nombre del fichero,
		 CanalEntrada= new BufferedReader(new InputStreamReader(usrConnection.getInputStream()));
		 FileWriter destino = new FileWriter("./src/io/PractEcho.java");
		 BufferedWriter brdestino = new BufferedWriter(destino);
		 String leido;
		 while ( (leido = CanalEntrada.readLine()) != null) {
		  brdestino.write(leido);
		  brdestino.newLine();
		  brdestino.flush();
		 }
		 
		 destino.close();
		 CanalEntrada.close();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			remoteCopy(usrConnection);
			usrConnection.close();
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
	}
	

}
