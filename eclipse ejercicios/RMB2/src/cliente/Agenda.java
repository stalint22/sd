package cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Agenda implements IAgenda {
	int idObject = -1;
	
	public Agenda() {		
		
		try {
			Socket sc = new Socket("localhost",1234);
		
			DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
			DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
			
			//Eleccion de operaci�n crear tabla
			canalSalida.writeInt(1);
			
			//Si el servidor lo autoriza leo el identificador de objeto
			if(canalEntrada.readBoolean()){
				idObject = canalEntrada.readInt();
				System.out.println("Dado de alta objeto con identificador: " + idObject);
			}
			//Si el servidor devuelve falsae quiere decir que el cliente ya
			//esta dado de alta en el sistema, y ya dispone de un identificador.
			else{
				System.out.println("La tabla ya est� creada");
			}
			
			sc.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
		public void asociar(String d, int v) {

			try{
			Socket sc = new Socket("localhost",1234);
			DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
			DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
			
			//El orden es primero comunicar el identificador
			canalSalida.writeInt(idObject);
			//2� elegir la operaci�n
			canalSalida.writeInt(2);
			//3� pasar par�metros
			canalSalida.writeUTF(d);
			canalSalida.writeInt(v);
			//4� leer flag true			
			if(canalEntrada.readBoolean())
				System.out.println("Asociado correctamente en la tabla "+idObject+ " : "+d+" - "+v);
			
			sc.close();
			}catch (Exception e) {
				System.out.println(e.toString());
			}
		
		}

		public int obtener(String d) {
		
			try {
				Socket sc = new Socket("localhost",1234);
				DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
				DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
				
				//1� Comunicar identificador
				canalSalida.writeInt(idObject);
				//2� elegir operaci�n
				canalSalida.writeInt(3);
				//3� pasar parametros
				canalSalida.writeUTF(d);
				//4� leer resultado
				int clave = canalEntrada.readInt();
				//5� leer true y devolver rsultado
				if(canalEntrada.readBoolean()){
					sc.close();
					return clave;
				}
				sc.close();
				return 0;
			} catch (Exception e) {
				e.printStackTrace();
			return 0;
			}
		}
}
