package servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Hashtable;

public class dispatchAgenda {

	// Contador de objetos creados, inicialmente 0
	int regObjetos = 0;
	int codOp = -1;
	Hashtable<Integer, Object> objTable = new Hashtable<Integer, Object>();
	DataOutputStream canalSalida;
	DataInputStream canalEntrada;	
	
	public void atender(Socket sc){
		int idObjeto = -1;
		try {
			// Obtengo las referencias a los canales de E/S del socket
			canalEntrada = new DataInputStream(sc.getInputStream());
			canalSalida = new DataOutputStream(sc.getOutputStream());
			
			// 1� Leo el identificador de objeto
			idObjeto = canalEntrada.readInt();
			// Si el identificador es 0, quiere decir que el cliente aun
			// no tiene tabla, asique asigno 0 al codigo de operacion
			if (idObjeto==0) codOp = 1;
			else
				// Si el idObjeto es distinto de 0, leo el codigo de operacion
				codOp = canalEntrada.readInt();
				switch (codOp) {
				case 1:
				// Si el objeto del tipo agenda asociado al idObjeto no esta creado
				// creo la entrada en la tabla hash y devuelvo el identificador
				// de objeto al cliente
				if (objTable.get(idObjeto) == null) {
					regObjetos++;
					// Insercion de la tupla Identificador(Integer)-Agenda
					objTable.put(new Integer(regObjetos),new Agenda());
					// Autorizo para que el cliente lea su identificador de objeto
					canalSalida.writeBoolean(true);
					// Le paso su identificador de objeto
					canalSalida.writeInt(regObjetos);
					// Si la entrada ya esta creada. devuelvo un false al
					// cliente.
				} else {
					canalSalida.writeBoolean(false);
				}
				break;
				case 2:
				// Operaci�n asociar
				System.out.println("Asociando en la tabla del objeto "+ idObjeto);
				
				// 1� Obtener la agenda asociada al identificador de objeto
				// 2� Llamar al m�todo asociar por medio de su agenda correspondiente
				
				((Agenda) objTable.get(idObjeto)).asociar(canalEntrada.readUTF(),new Integer(canalEntrada.readInt()));
								
				// 3� Devolver true
				canalSalida.writeBoolean(true);
				break;
				case 3:
				// Operaci�n obtener
				System.out.println("Consultando tabla del objeto "+ idObjeto);
				// 1� Obtenemos la agenda asociada al identificador
				// 2� Llamamos al metodo obtener
				// pasando el parametro recibido del cliente
				// El resultado de la operacion se escribe en el canal de salida
				
				canalSalida.writeInt(((Agenda) objTable.get(idObjeto)).obtener(canalEntrada.readUTF()));
				
				
				// 3� Devolver true
				canalSalida.writeBoolean(true);
				break;
				}
				sc.close();
		} catch (IOException e) {
		System.out.println(e.toString());
		}
		}
	}


