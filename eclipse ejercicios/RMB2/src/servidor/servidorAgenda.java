package servidor;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class servidorAgenda {
	ServerSocket s;
	Socket sc;
	dispatchAgenda dispatch;
	
	// Constructor de la clase Servidor
	// Cuando se instancie el servidor se crear� un objeto de la clase Dispatch
	// que ser� el que atender� las conexiones recibidas en el servidor
	
	public  servidorAgenda(int port) {  
		
		try{
			s = new ServerSocket(port);
			dispatch = new dispatchAgenda();
			System.out.println("Iniciando servidor...");
			} catch (IOException e) {
			System.out.println(e.toString());
			}
	}
	
	

	public void establecer (){
		try {
		sc = s.accept();
		dispatch.atender(sc);
		sc.close();
		} catch (IOException e) {
		System.out.println(e.toString());
		}
		}
		
	public static void main(String[] args) {
		//Instanciamos un �nico Servidor para gestionar las conexiones entrantes
		servidorAgenda s = new servidorAgenda(1234);
		//Continuamente estar� intentado establecer conexiones
		while (true) s.establecer();
		}
	}

	
	


