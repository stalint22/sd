package cliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Agenda implements IAgenda{
	Socket sc;
	String host = "localhost";
	int port = 9999;
	Agenda() {
		try {
			sc = new Socket(host, port);
			DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
			DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(1);
			if(canalEntrada.readBoolean()){
				System.out.println("Creaci�n de la tabla -----> Todo OK");
			}
			else
				System.out.println("Creacion de la tablas ---->NO OK");
			
			sc.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void asociar(String s, int v){
		try {
			sc = new Socket(host, port);
			DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
			DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(2);
			canalSalida.writeUTF(s);
			canalSalida.writeInt(v);
			if(canalEntrada.readBoolean()){
				System.out.println("Insercci�n en la tabla (asociar) ---> Todo ok");
			}
			else System.out.println("Insercci�n en la tabla (asociar)---> No OK");
			
			sc.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int obtener(String s){
			int valor;
		try {
			sc = new Socket(host, port);
			DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
			DataInputStream canalaEntrada = new DataInputStream(sc.getInputStream());
			
			canalSalida.writeInt(3);
			canalSalida.writeUTF(s);
			valor = canalaEntrada.readInt();
			
			if(canalaEntrada.readBoolean()){
				System.out.println("Consulta en la tabla(obtener) --->Tdo OK");
			}
			else System.out.println("Consulta en la tabla (obtener)--->No OK");
			
			sc.close();
		} catch (Exception e) {
			e.printStackTrace();
			valor = -1;
		}	
		return (valor);
	}
}
