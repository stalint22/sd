package cliente;
/*
 * Interfaz repositorio
 */
public interface IAgenda {
	//asocia un valor entero a una cadena
	public void asociar(String s, int v);
	//obtiene el valor entero asociado previamente a la
	//cadena especificada
	public int obtener(String s);
}
