package servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class servidorAgenda {

	public static void main(String[] args) {
		ServerSocket ss;
		Socket sc;
		int codOp;
		boolean existeTabla = false;
		Agenda agenda = null;
		try {
			ss = new ServerSocket(9999);
			while(true){
				sc = ss.accept();
				DataOutputStream canalSalida = new DataOutputStream(sc.getOutputStream());
				DataInputStream canalEntrada = new DataInputStream(sc.getInputStream());
				codOp = canalEntrada.readInt();
				//traza
				System.out.println("recibo codigo de operacion: "+ codOp);
				
				switch (codOp) {
				case 1:
						agenda = new Agenda();
						existeTabla = true;
						canalSalida.writeBoolean(true);
					
					break;
				
				case 2:
						if(existeTabla){
							agenda.asociar(canalEntrada.readUTF(), canalEntrada.readInt());
							canalSalida.writeBoolean(true);
						}else{
							canalSalida.writeBoolean(false);
							System.out.println("No existe tabla");
						}
					break;
				case 3:
						if(existeTabla){
							canalSalida.writeInt(agenda.obtener(canalEntrada.readUTF()));
							canalSalida.writeBoolean(true);
						}else{
							canalSalida.writeBoolean(false);
							System.out.println("No existe la tabla");
							
						}
					
					break;

				default:
					System.out.println("El c�digo " + codOp+ " no es correcto");
					break;
				}
				
				//traza
				System.out.println("Ya se ha trazado "+ codOp);
				
			}
			//sc.close();
			//ss.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
