import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class ServerSocketTCP {

	public static void main(String[] args) throws IOException {
		ServerSocket server = new ServerSocket(10001);
		
		InputStream is;
        OutputStream os;
		DataOutputStream salida;
		while (true){
			Socket socket = server.accept();
			//Almacenamos lo que nos pida el cliente
			is = socket.getInputStream();
			os = socket.getOutputStream();
			
			// lee lo que esta llegando del cliente
			BufferedReader entrada = new BufferedReader(new InputStreamReader(is));
			//guarda en salida los datos
			PrintWriter outToclient = new PrintWriter(os, true);
        	String mensaje = entrada.readLine();
	        while( mensaje != null ){
	        	System.out.println(mensaje);
	        	outToclient.println(mensaje);
	        		            			
			}
			socket.close();
		}

	}

}
