package Servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class ThreadSever implements Runnable {
	
	Socket sc;
	ISkeleton sk;
	DataInputStream canalEntrada;
	DataOutputStream canalSalida;
	public ThreadSever(Socket sc , ISkeleton sk) {
		this.sc = sc;
		this.sk = sk;
		
	}
	
	public void run(){
		sk.process(canalEntrada, canalSalida);
	}

}
